result 1:

          appliance:   true power:  reactive power:    phase:
              Fridge        -67.14            -5.28         1
              Fridge         77.41             3.13         1
             Freezer        -52.33            18.99         1
             Freezer         67.01           -21.41         1
          Dishwasher      -2071.57            -6.48         1
          Dishwasher       2078.17            -3.21         1
        Water kettle      -1856.50             0.64         1
        Water kettle       1876.72             1.74         1
                Lamp        -92.97            -1.55         1
                Lamp        -89.51          -122.44         1
                Lamp        249.71            87.53         1
                Lamp        145.35           107.37         1
              Laptop        -20.77            -1.38         1
              Laptop         34.23             2.61         1
                  TV       -163.81           -32.38         2
                  TV        179.32            40.19         2
              Stereo        -36.02           -35.36         2
              Stereo        226.30            56.84         2


Consumption:
Fridge: 
    ResultNr     Fscore    Precision     Recall        TPR        FPR    DeviationPct        RMS        NPE
           1     0.9582       0.9656     0.9510     0.9510     0.0136          0.0161    26.5677     0.1789

Events:
Fridge: 
  ResultNr     Fscore  Precision     Recall        Appliance FP_Fraction
         1     0.9252     0.9864     0.8711           Fridge     0.0000
