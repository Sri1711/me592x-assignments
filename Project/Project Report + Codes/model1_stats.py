# -*- coding: utf-8 -*-
"""
Created on Mon Apr 16 19:20:46 2018

@author: srjsingh17
"""

import pandas as pd
import os
import matplotlib.pyplot as plt
import warnings
warnings.filterwarnings('ignore')
import seaborn as sns
sns.set_style("darkgrid")
from statsmodels.tsa.arima_model import ARIMA
from sklearn.metrics import mean_squared_error
import statsmodels.api as sm
from statsmodels.tsa.stattools import adfuller

#Data import, time stamp generation and aggregation
os.chdir('C:\\Users\\srjsingh17\\Desktop\\Sem 4\\ME 592X\\Project')
df = pd.read_csv('EMHIRESPV_TSh_CF_Country_19862015.csv')
df.head(3)
df.shape
t = pd.date_range('1/1/1986', periods = 262968, freq = 'H')
df["Hour"] = t
df.head(10)
#df['Day']=df['Hour'].map(lambda x: x.strftime('%Y-%m-%d'))
df['Month']=df['Hour'].map(lambda x: x.strftime('%Y-%m'))
df_grouped = df.groupby('Month').mean()
df_grouped = df_grouped.reset_index()
df_grouped.head(10)

#ACF and PACF plot
from statsmodels.graphics.tsaplots import plot_acf
plot_acf(df_grouped['UK'], lags = 10)
from statsmodels.graphics.tsaplots import plot_pacf
plot_pacf(df_grouped['UK'], lags = 10)

#ADF unit root test
ts = pd.Series(df_grouped['UK'].values, index=df_grouped['Month'])
result = adfuller(ts)
print('ADF Statistic: %f' % result[0])
print('p-value: %f' % result[1])
print('Critical Values:')
for key, value in result[4].items():
	print('\t%s: %.3f' % (key, value))

#ARIMA
df_train = df_grouped.iloc[0:-36]
df_test = df_grouped.iloc[-36:]
train = pd.Series(df_train['UK'].values, index=df_train['Month'])
test = pd.Series(df_test['UK'].values, index=df_test['Month'])
history = [x for x in train]
predictions = list()
for t in range(len(test)):
	model = ARIMA(history, order=(4,0,0))
	model_fit = model.fit(disp=0)
	output = model_fit.forecast()
	yhat = output[0]
	predictions.append(yhat)
	obs = test[t]
	history.append(obs)
	print('predicted=%f, expected=%f' % (yhat, obs))
error = mean_squared_error(test, predictions)
print('Test MSE: %.7f' % error)
pred = pd.Series(x[0] for x in predictions)
test_value = pd.Series(df_test['UK'].values)
fig, ax = plt.subplots()
ax.plot(test_value, 'b-', label='test')
ax.plot(pred, 'r-', label='predicted')
plt.plot(test_value)
plt.plot(pred, color='red')

#SARIMA
df_train = df_grouped.iloc[0:-36]
df_test = df_grouped.iloc[-36:]
train = pd.Series(df_train['UK'].values, index=df_train['Month'])
test = pd.Series(df_test['UK'].values, index=df_test['Month'])
history = [x for x in train]
predictions = list()
for t in range(len(test)):
	model = sm.tsa.statespace.SARIMAX(history, order=(0,0,2), seasonal_order=(4,1,0,12))
	model_fit = model.fit(disp=0)
	output = model_fit.forecast()
	yhat = output[0]
	predictions.append(yhat)
	obs = test[t]
	history.append(obs)
	print('predicted=%f, expected=%f' % (yhat, obs))
error = mean_squared_error(test, predictions)
print('Test MSE: %.7f' % error)
pred = pd.Series(predictions)
test_value = pd.Series(df_test['UK'].values)
plt.plot(test_value)
plt.plot(pred, color='red')

#Similarly, the whole process is done for daily prediction, aggregating the hourly data on daily basis instead of monthly basis.
#The model is estimated for different combination of parameters.


