# -*- coding: utf-8 -*-
import pandas as pd
import numpy as np
import datetime as dt
import matplotlib.pyplot as plt
import matplotlib.dates as pltdt
import hmmlearn
from hmmlearn.hmm import GaussianHMM

coun = pd.read_csv("C:/Users/hp/.spyder-py3/EMHIRESPV_TSh_CF_Country_19862015.csv")
coun_arr = coun.values
coun_arr = coun_arr[:,28:29] # making sure it is only UK
train_coun_arr = coun_arr[:210375]
test_coun_arr = coun_arr[210376:]
coun_uk = coun.iloc[:,28:29]

t = pd.date_range('1/1/1986', periods = 262968, freq = 'H')
coun["Hour"] = t
coun.set_index("Hour", inplace = True)
uk_nn = coun.filter(['Hour','UK'], axis = 1)

uk_nn = uk_nn.reset_index()
uk_nn['Hour'] = pd.to_datetime(uk_nn['Hour'])
start = pd.Timestamp('2015-01-01')
split = pd.Timestamp('2015-08-22')
uk_nn = uk_nn[uk_nn['Hour']>=start]

uk_nn = uk_nn.set_index('Hour')

train = uk_nn.loc[:split, ['UK']]
test = uk_nn.loc[split:, ['UK']]
#print(train.shape())
# Train HMM with 80% UK data
model = GaussianHMM(n_components=2, covariance_type="diag",random_state=None, n_iter=1).fit([train_coun_arr])

print('Transition matrix size:',model.transmat_.shape )
print(model.transmat_)

#Evaluation with train_coun_arr
hidden_states_model = model.predict(test_coun_arr)
hidden_states_model_score = model.score(test_coun_arr)
print('hidden_states_model_score:',hidden_states_model_score)

#plt.plot(test_coun_arr)
plt.plot(hidden_states_model)











