# -*- coding: utf-8 -*-
import pandas as pd
import numpy as np
import datetime as dt

import matplotlib.pyplot as plt
import matplotlib.dates as pltdt

import seaborn as sns
sns.set_style("darkgrid")

coun = pd.read_csv("C:/Users/hp/.spyder-py3/EMHIRESPV_TSh_CF_Country_19862015.csv")
print(coun.head(3))

t = pd.date_range('1/1/1986', periods = 262968, freq = 'H')
coun["Hour"] = t
coun.set_index("Hour", inplace = True)
#coun['2015-12-31'].plot()
#plt.legend(bbox_to_anchor=(1.05, 1), loc=2, ncol = 2,  borderaxespad=0)

#coun['2015-12'].plot()
#plt.legend(bbox_to_anchor=(1.05, 1), loc=2, ncol = 2,  borderaxespad=0)

coun['Day']=coun.index.map(lambda x: x.strftime('%Y-%m-%d'))
c_group_day = coun.groupby('Day').mean()
#c_group_day.plot()
#plt.legend(bbox_to_anchor=(1.05, 1), loc=2, ncol = 2,  borderaxespad=0.)

coun['Month']=coun.index.map(lambda x: x.strftime('%Y-%m'))
coun['Month_only']=coun.index.map(lambda x: x.strftime('%m'))
c_group_month = coun.groupby('Month').mean()
#c_group_month.plot()

coun['Year']=coun.index.map(lambda x: x.strftime('%Y'))
c_group_year = coun.groupby('Year').mean()
#c_group_year.plot()
#plt.legend(bbox_to_anchor=(1.05, 1), loc=2, ncol = 2,  borderaxespad=0.)

uk_heatmap =coun.pivot_table(index = 'Month_only', columns = 'Year', values = 'UK')
uk_heatmap.sortlevel(level = 0, ascending = True, inplace = True)
#sns.heatmap(pt_heatmap, vmin = 0.09, vmax = 0.29, cmap = 'inferno', linewidth = 0.5)
#sns.clustermap(pt_heatmap, cmap = 'inferno', standard_scale = 1)

uk_ts = coun.filter(['Month','Year','UK'], axis = 1)
#uk_ts.plot()

uk_ts_m = uk_ts.groupby('Month').mean()
#uk_ts_m.plot()

uk_ts_y = uk_ts.groupby('Year').mean()
#uk_ts_y.plot()

# predict next hour energy potential using RNN
uk_nn = coun.filter(['Hour','UK'], axis = 1)

uk_nn = uk_nn.reset_index()
uk_nn['Hour'] = pd.to_datetime(uk_nn['Hour'])
start = pd.Timestamp('2015-01-01')
split = pd.Timestamp('2015-08-22')
uk_nn = uk_nn[uk_nn['Hour']>=start]

uk_nn = uk_nn.set_index('Hour')
#uk_nn.plot()

train = uk_nn.loc[:split, ['UK']]
test = uk_nn.loc[split:, ['UK']]
tr_pl = train
te_pl = test
#ax = tr_pl.plot()
#te_pl.plot(ax=ax)

from sklearn.preprocessing import StandardScaler

sc = StandardScaler()
train_sc = sc.fit_transform(train)
test_sc = sc.transform(test)

X_train = train_sc[:-1]
y_train = X_train[1:]
X_train = X_train[:-1]

X_test = test_sc[:-1]
y_test = X_test[1:]
X_test = X_test[:-1]

train_df = pd.DataFrame(train_sc, columns = ['UK'], index = train.index)
test_df = pd.DataFrame(test_sc, columns = ['UK'], index = test.index)
#ROLLING WINDOWS
for s in range(1,25):
    train_df['shift{}'.format(s)] = train_df['UK'].shift(s, freq = 'H')
    test_df['shift{}'.format(s)] = test_df['UK'].shift(s, freq = 'H')
train_df.head(3)
#GET RID ON NaN
X_train = train_df.dropna().drop('UK', axis = 1)
y_train = train_df.dropna()[['UK']]

X_test = test_df.dropna().drop('UK', axis = 1)
y_test = test_df.dropna()[['UK']]
#X_train.head(3)

X_train = X_train.values #dataframe to float64
y_train = y_train.values

X_test = X_test.values
y_test = y_test.values

#PREDICTIVE MODEL 
X_train_w = X_train.reshape(X_train.shape[0], 1, 24)
X_test_w = X_test.reshape(X_test.shape[0], 1, 24)
#X_train_w.shape()

from keras.models import Sequential
from keras.layers import Dense, LSTM, Flatten
from keras.optimizers import Adam
from keras.callbacks import EarlyStopping
import keras.backend as K

K.clear_session()
eps = 500
bs = 1

in_sh = (1,24)
hidden_1 = 12
hidden_2 = 12
outputs = 1

model = Sequential()
model.add(LSTM(hidden_1, input_shape = in_sh))
model.add(Dense(hidden_2, activation ='relu'))
model.add(Dense(outputs))
model.compile(optimizer='adam', loss='mean_squared_error')
model.summary()

early_stop = EarlyStopping(monitor = 'loss', patience = 1, verbose = 1)
model.fit(X_train_w, y_train, epochs = eps, batch_size = bs, verbose = 1 , callbacks = [early_stop])

y_pred = model.predict(X_test_w)

plt.plot(y_test)
plt.plot(y_pred)




