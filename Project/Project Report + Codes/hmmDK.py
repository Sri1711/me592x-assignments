# -*- coding: utf-8 -*-
"""
Created on Tue May  1 16:13:12 2018

@author: hp
"""

import pandas as pd
import numpy as np
import datetime as dt
import matplotlib.pyplot as plt
import matplotlib.dates as pltdt
import hmmlearn
from hmmlearn.hmm import GaussianHMM

coun = pd.read_csv("C:/Users/hp/.spyder-py3/EMHIRESPV_TSh_CF_Country_19862015.csv")
coun_arr = coun.values
coun_arr = coun_arr[:,7:8] # making sure it is only DK
train_coun_arr = coun_arr[:210375]
test_coun_arr = coun_arr[210376:]
coun_dk = coun.iloc[:,7:8]

model = GaussianHMM(n_components=2, covariance_type="diag",random_state=None, n_iter=1).fit([train_coun_arr])

print('Transition matrix size:',model.transmat_.shape )
print(model.transmat_)

#Evaluation with train_coun_arr
hidden_states_model = model.predict(test_coun_arr)
hidden_states_model_score = model.score(test_coun_arr)
print('hidden_states_model_score:',hidden_states_model_score)

#plt.plot(test_coun_arr)
plt.plot(hidden_states_model)
